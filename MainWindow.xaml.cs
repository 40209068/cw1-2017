﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CsharpCoursework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Attendee p1 = new Attendee();
        private void set_button_Click(object sender, RoutedEventArgs e)
        {
            //set textbox's
            p1.Fname = FnameTextBox.Text;
            p1.Lname = LnameTextBox.Text;
            p1.Attendeeref = AttendeerefTextBox.Text;
            p1.Institution = InstitutionTextBox.Text;
            p1.Conference = ConferenceTextBox.Text;
            p1.Papertitle = PapertitleTextBox.Text;

            //set combobox's
            p1.Registration = RegTypecomboBox.Text;

            //set checkbox's
            p1.Paid = PaidcheckBox.IsChecked.Value;
            p1.Presenter = PresentercheckBox.IsChecked.Value;
        }

        private void get_button_Click(object sender, RoutedEventArgs e)
        {
            //get textbox's
            FnameTextBox.Text = p1.Fname;
            LnameTextBox.Text = p1.Lname;
            AttendeerefTextBox.Text = p1.Attendeeref;
            InstitutionTextBox.Text = p1.Institution;
            ConferenceTextBox.Text = p1.Conference;
            PapertitleTextBox.Text = p1.Papertitle;
            
            //get combobox
            RegTypecomboBox.Text = p1.Registration;

            //get checkbox's
            PaidcheckBox.IsChecked = p1.Paid;
            PresentercheckBox.IsChecked = p1.Presenter;
        }

        private void clear_button_Click(object sender, RoutedEventArgs e)
        {
            //clear textbox's
            FnameTextBox.Text = string.Empty;
            LnameTextBox.Text = string.Empty;
            AttendeerefTextBox.Text = string.Empty;
            InstitutionTextBox.Text = string.Empty;
            ConferenceTextBox.Text = string.Empty;
            PapertitleTextBox.Text = string.Empty;

            //clear combobox
            RegTypecomboBox.SelectedItem = null;

            //clear checkbox's
            PaidcheckBox.IsChecked = false;
            PresentercheckBox.IsChecked = false;
        }

        private void invoice_button_Click(object sender, RoutedEventArgs e)
        {
            //opening invoice window
            Window1 win1 = new Window1();
            win1.Show(ref p1);
            this.Close();
        }

        private void certificate_button_Click(object sender, RoutedEventArgs e)
        {
            //opening certificate window
            Window2 win2 = new Window2();
            win2.Show(ref p1);
            this.Close();
        }
    }
}
