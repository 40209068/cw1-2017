﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CsharpCoursework
{
    public class Attendee
    {
        private string attendeeref;

            public string Attendeeref
            {
                get { return this.attendeeref;
            }
                set { this.attendeeref = value; 
            }
            }
        
            private string fname;
            public string Fname
            {
                get { return this.fname; 
            }
                set { this.fname = value; 
            }
            }

            private string lname;
            public string Lname
            {
                get { return this.lname; 
            }
                set { this.lname = value; 
            }
            }

            private string institution;
            public string Institution
            {
                get { return this.institution; 
            }
                set { this.institution = value; 
            }
            }

            private string conference;
            public string Conference
            {
                get { return this.conference; 
            }
                set { this.conference = value; 
            }
            }

            private string registration;
            public string Registration
            {
                get { return this.registration; 
            }
                set { this.registration = value; 
            }
            }

            private Boolean paid;
            public Boolean Paid
            {
                get { return this.paid; 
            }
                set { this.paid = value; 
            }
            }

            private Boolean presenter;
            public Boolean Presenter
            {
                get { return this.presenter; 
            }
                set { this.presenter = value; 
            }
            }

            private string papertitle;
            public string Papertitle
            {
                get { return this.papertitle; 
            }
                set { this.papertitle = value; 
            }
            }

        //getting the double set up
        public double GetCost()
        {
            double Cost = 0;
            if (Registration == "Full")
            {
                Cost = 500;
            }
            else if (Registration == "Student")
            {
                Cost = 300;
            }
            else if (Registration == "Organiser")
            {
                Cost = 0;
            }
            
            if (Presenter == true)
            {
                Cost = Cost / 100 * 90;
            }

            return Cost;
        }
        
    }
}
