﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CsharpCoursework
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        public Window2()
        {
            InitializeComponent();
        }

        public void Show(ref Attendee p1)
        {
            //setting up the label
            Label my_label = new Label();
            my_label.Name = "my_label";
            string the_label;
            //setting the label to change if presenter is checked
            if (p1.Presenter)
            {
                the_label = String.Format("This is to certify that {0} {1} attended {2} and presented a paper entitled {3}", p1.Fname, p1.Lname, p1.Conference, p1.Papertitle);
            }
            else
            {
                the_label = String.Format("This is to certify that {0} {1} attended {2}", p1.Fname, p1.Lname, p1.Conference);
            }
            my_label.Content = the_label;

            //setting label to the grid
            Grid.SetRow(my_label, 0);
            Grid.SetColumn(my_label, 0);
            Certificategrid.Children.Add(my_label);
            base.Show();
        }
        private void button_Click(object sender, RoutedEventArgs e)
        {
            //closing that window to open the first
            MainWindow win1 = new MainWindow();
            win1.Show();
            this.Close();
        }
    }
}
